class Motor {
    constructor(merk, tahun){
        this.merk = merk
        this.tahun = tahun
    }

    mesin(){
        console.log("Mesin 4tak")
    }

    jok(){
        console.log("Jok motor ", this.merk, "empuk!")
    }

}

class Roda2 extends Motor {
    constructor(merk, tahun, tinggi){
        super(merk, tahun);
        this.tinggi = tinggi
    }

    service(){
        console.log("Harap di service 3 bulan sekali...")
    }

    knalpot(){"Tinggi motor lebih dari ", this.tinggi, "knalpot racing."}
}

let Supra = new Motor("Honda", 2000)
let SupraX = new Roda2("Honda", 2010, 150)
console.log(SupraX)



var nama = "Giyanaryoga"
console.log(nama)

console.sort

class Tumbuhan {
    constructor(jenis, daun, media_tanam, akar, penyakit){
        this.jenis = jenis
        this.daun = daun
        this.media_tanam = media_tanam
        this.akar = akar
        this.penyakit = penyakit
    }

    Obat(){
        console.log("Obat insektisida untuk penyakit ", this.penyakit)
    }    

    Siram(){
        console.log("Siram saat ", this.media_tanam, " tidak lembab")
    }
}

var Mangga = new Tumbuhan("Mangga Arum Manis", "daun panjang", "tanah", "akar tunggang","Daun Kuning")
var Jeruk = new Tumbuhan("Jeruk purut", "daun 8", "tanah", "akar tunggang", )

console.log(Mangga.penyakit)
console.log(Jeruk.Siram)
Jeruk.Siram()
