import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import { Container } from "reactstrap";

import Nav from "../components/navbar";
import firebase from "../services/firebase";
import Button from "../components/button";

const storage = firebase.storage();
class Contact extends Component {
  state = {};

  handleOnChanges = (e) => {
    const file = e.target.files[0];
    this.setState({ file });
    const reader = new FileReader();
    reader.addEventListener(
      "load",
      () => {
        this.setState({ image: reader.result });
      },
      false
    );
    if (file.type.include("image/")) reader.readAsDataURL(file);
  };

  handleSubmit = async (e) => {
    e.preventDefault();
    const { file } = this.state;

    try {
      const response = await storage.ref(`files/${file.name}`).put(file);
      console.log(response);
      alert("File uploaded!");
    } catch (err) {
      console.log(err);
      alert("Failed to upload file!");
    }
  };

  render() {
    return (
      <Fragment>
        <Nav />
        <Container>
          {!!this.state.image && (
            <img
              src={this.state.image}
              alt="Preview"
              height="40%"
              width="20%"
            />
          )}
          <br />
          <input type="file" onChange={this.handleOnChanges} />
          <br />
          <Button title="Uploade" onclick={this.handleSubmit} color="info" />
        </Container>
      </Fragment>
    );
  }
}

export default Contact;
