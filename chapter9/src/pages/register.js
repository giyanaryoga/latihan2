import React, { Component } from "react";
import { Container, Form } from "reactstrap";

import firebase from "../services/firebase";
import Button from "../components/button";

class Register extends Component {
  state = {
    email: "",
    password: "",
  };

  handleSubmit = (e) => {
    e.preventDefault();
    const { email, password } = this.state;

    if (!email || !password) alert("Mohon masukkan email dan password");

    firebase.auth().createUserWithEmailAndPassword(email, password);
    alert("Sukses terdaftar!");
  };

  set = (name) => (event) => {
    e.preventDefault();
    this.setState({
      [name]: event.target.value,
    });
  };
  render() {
    return (
      <Container>
        <Form>
          <input type="text" placeholder="email" onChange={this.set("email")} />
          <br />
          <br />
          <input
            type="password"
            placeholder="password"
            onChange={this.set("password")}
          />
          <br />
          <br />
          <Button title="Register" color="primary" click={this.handleSubmit} />
        </Form>
      </Container>
    );
  }
}

export default Register;
