import React, { Component } from "react";

import Nav from "../components/navbar";
import Button from "../components/button";
import Post from "../components/post";
import { Container } from "reactstrap";

class Home extends Component {
  state = {
    todo: [],
  };

  componentDidMount() {
    fetch("https://jsonplaceholder.typicode.com/todos")
      .then((response) => response.json())
      .then((result) => {
        this.setState({ todo: result });
      });
  }

  render() {
    return (
      <>
        <Nav />
        <Container>
          <h1>List to-do: </h1>
          {this.state.todo.map((element) => {
            return <Post title={element.title} id={element.id} />;
          })}
        </Container>
      </>
    );
  }
}

export default Home;
