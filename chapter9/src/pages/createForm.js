import React, { Component } from "react";
import { Container, FormGroup, Input, Label } from "reactstrap";

import Button from "../components/button";
import firebase from "../services/firebase";

class CreateForm extends Component {
  state = {
    username: "",
    email: "",
    nama: "",
    alamat: "",
  };

  set = (name) => (event) => {
    event.preventDefault();
    this.setState({
      [name]: event.target.value,
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    const { username, email, nama, alamat } = this.state;
    firebase
      .database()
      .ref("user/" + username)
      .set({
        username,
        email,
        nama,
        alamat,
      });
  };
  render() {
    return (
      <Container>
        <FormGroup>
          <Label for="email">Email</Label>
          <Input
            type="email"
            name="email"
            id="email"
            placeholder="Email"
            onChange={this.set("email")}
          />
        </FormGroup>
        <FormGroup>
          <Label for="username">Username</Label>
          <Input
            type="username"
            name="username"
            id="username"
            placeholder="Username"
            onChange={this.set("username")}
          />
        </FormGroup>
        <FormGroup>
          <Label for="nama">Nama</Label>
          <Input
            type="nama"
            name="nama"
            id="nama"
            placeholder="Nama"
            onChange={this.set("nama")}
          />
        </FormGroup>
        <FormGroup>
          <Label for="alamat">Alamat</Label>
          <Input
            type="alamat"
            name="alamat"
            id="alamat"
            placeholder="Alamat"
            onChange={this.set("alamat")}
          />
        </FormGroup>

        <Button title="Add Data" color="primary" click={this.handleSubmit} />
      </Container>
    );
  }
}

export default CreateForm;
