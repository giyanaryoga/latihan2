import firebase from "firebase";

import "firebase/auth";
import "firebase/storage";

let firebaseConfig = {
  apiKey: "AIzaSyA-aQFTj4sN5psEu4xFhS4ZXcgH2ktQcdY",
  authDomain: "binar-challenge-9-fad6c.firebaseapp.com",
  projectId: "binar-challenge-9-fad6c",
  storageBucket: "binar-challenge-9-fad6c.appspot.com",
  messagingSenderId: "587941738571",
  appId: "1:587941738571:web:127e30f14591dd352475ce",
  measurementId: "G-DCLGWY0P0S",
};

//Initialize firebase
const app = firebase.initializeApp(firebaseConfig);

export default app;
