import React from "react";
import { Button } from "reactstrap";

const ButtonCreator = (props) => {
  return (
    <>
      <Button color={props.color} onClick={props.click}>
        {props.title}
      </Button>
    </>
  );
};

export default ButtonCreator;
