const express = require('express')
const { filter } = require('methods')
const router = express.Router()

let dataProduct = [
    {
        id : "p1laptop",
        namaProduct : "Laptop ROG",
        namaBrand : "ASUS", 
        jmlKetersediaan : 100
    },
    {
        id : "p2laptop",
        namaProduct : "Macbook",
        namaBrand : "Apple", 
        jmlKetersediaan : 700
    },
    {
        id : "p3laptop",
        namaProduct : "Laptop Gamer",
        namaBrand : "Lenovo", 
        jmlKetersediaan : 200
    },
    {
        id : "p4laptop",
        namaProduct : "Laptop Design",
        namaBrand : "ACER", 
        jmlKetersediaan : 400
    },
    {
        id : "p5laptop",
        namaProduct : "Laptop Touchscreen",
        namaBrand : "HP", 
        jmlKetersediaan : 500
    },
    {
        id : "p6laptop",
        namaProduct : "Laptop Pintar",
        namaBrand : "Samsung", 
        jmlKetersediaan : 1000
    },
]

router.post('/api/product', (req, res) => {
    const {namaProduct, namaBrand, jmlKetersediaan} = req.body

    //validation
    if (namaProduct === undefined || namaBrand === undefined || jmlKetersediaan === undefined){
        res.status(400).json("Bad request!")
        return
    }

    //id = p[produk]+nomor id+laptop
    const nextId = dataProduct.length + 1
    const  id = "p"+nextId+"laptop"

    //Destructive Method
    // const {namaProduct, jumlahProduct} = req.body
    
    dataProduct.push({
        id : id,
        namaProduct : namaProduct,
        namaBrand : namaBrand,
        jmlKetersediaan : jmlKetersediaan
    })
    
    res.status(200).json('Succed!')
})

router.get('/api/product', (req, res) => {
    res.status(200).json(dataProduct)
})

router.get('/api/product/:id', (req, res) => {
    const id = req.params.id
    const filterProduct = dataProduct.find(i => i.id == id)

    if (filterProduct === undefined){
        res.status(400).json("Data Product is not found")
        return
    }
    res.status(200).json(filterProduct)
})

router.put('/api/product/:id', (req, res) => {
    const id = req.params.id
    const {namaProduct, namaBrand, jmlKetersediaan} = req.body

    if (namaProduct === undefined || namaBrand === undefined || jmlKetersediaan === undefined){
        res.status(400).json("Bad request!")
        return
    }

    let isFound = false
    for(let x = 0; x<dataProduct.length; x++){
        if (dataProduct[x].id == id){
            dataProduct[x].namaProduct = namaProduct
            dataProduct[x].namaBrand = namaBrand
            dataProduct[x].jmlKetersediaan = jmlKetersediaan
            isFound = true
            break
        }
    }

    if (isFound == true){
        res.status(200).json("Updated!")
        return
    } else {
        res.status(400).json("Data Product is not found!")
        return
    }
})

router.delete('/api/product/:id', (req, res) => {
    const id = req.params.id
    const searchProduct = dataProduct.find(i => i.id == id)

    if(searchProduct === undefined){
        res.status(404).json("Data product is not found!")
        return
    }

    const index = dataProduct.indexOf(searchProduct)

    dataProduct.splice(index,1)
    res.status(200).json("Deleted")
})

module.exports = router