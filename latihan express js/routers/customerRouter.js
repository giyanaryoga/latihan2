const express = require('express')
const router = express.Router()

let dataCustomer = [
    {
        id :1,
        nama : "Giyanaryoga",
        alamat : "Cirebon"
    },
    {
        id :2,
        nama : "Puguh",
        alamat : "Jakarta"
    },
    {
        id :3,
        nama : "Sanyoto",
        alamat : "Yogyakarta"
    }
]


router.post('/api/customer', (req, res) => {
    const {nama, alamat} = req.body

    if(nama === undefined || alamat === undefined){
        res.status(400).json("Bad request because some data is not sent")
        return
    }
    const nextId = dataCustomer.length + 1

    dataCustomer.push({
        id : nextId,
        nama : nama,
        alamat : alamat
    })

    res.status(200).json("Succeed!")
})

router.get('/api/customer', (req, res) => {
    res.status(200).json(dataCustomer)
})

router.get('/api/customer/:id', (req, res) => {
    const id = req.params.id
    const filteredCustomer = dataCustomer.find(x => x.id == id)

    // Validasi pencarian data
    if(filteredCustomer === undefined){
        res.status(404).json("Data customer is not found!")
        return
    }
    res.status(200).json(filteredCustomer)
})

router.put('/api/customer/:id', (req, res) => {
    const id = req.params.id
    const {nama, alamat} = req.body

    if(nama === undefined || alamat === undefined){
        res.status(400).json("Bad request because some data is not sent")
        return
    }

    let isFound = false
    for(var x = 0; x<dataCustomer.length; x++){
        if(dataCustomer[x].id == id){
            dataCustomer[x].nama = nama
            dataCustomer[x].alamat = alamat

            isFound = true
            break
        }
    }

    if(isFound == false){
        res.status(404).json("Data customer is not found!")
        return
    }else{
        res.status(200).json("Updated!")
        return
    }
})

router.delete('/api/customer/:id', (req, res) => {
    const id = req.params.id

    const searchCustomer = dataCustomer.find(x => x.id == id)

    if (searchCustomer === undefined){
        res.status(404).json("Data customer is not found!")
        return
    }
    
    // return index dari variable searchCustomer
    const index = dataCustomer.indexOf(searchCustomer)

    // Argument (start) = Ngehapus element array dari index berapa?
    // Argument (end) = Mau menghapus sebanyak berapa data?
    dataCustomer.splice(index,1)

    res.status(200).json("Deleted")
})

module.exports = router