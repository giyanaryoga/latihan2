const express = require('express')
const app = express()
const customerRouter = require('./routers/customerRouter')
const productRouter = require('./routers/productRouter')

app.use(express.json())

app.use(customerRouter)
app.use(productRouter)

app.listen(8080, () => {
    console.log(`Server is running!`);
});