const router = require('express').Router()
const authController = require('../controllers/authController')

router.get('/register', (req, res) => {
    res.render('register')
})

router.post('/register', authController.createUser)

router get('/login', (req, res) => {
    res.render('login')
})

router.post('/login', authController.authUser)

router.get('/', (req, res) => {
    res.render('index')
})

module.exports = router