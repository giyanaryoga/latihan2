const { User } = require('../models')

jwtResponsesFormatter = (user) => {
    
}

module.exports = {
    createUser : async (req, res) => {
        try {
            let userCreated = await User.register(req.body.username, req.body.password)

            res.redirect('/login')
        } catch(err) {
            console.log(err)
            res.json(500, "Error")
        }
    },

    authUser: async (req, res) => {
        try {
            let CheckUser = await User.authentication(req.body.username, req.body.password)

            if (CheckUser) {
                res.json(200, 'Berhasil Login')
            } else {
                res.json(400, 'Gagal Login')
            }
        } catch(err) {
            console.log(err)
            res.json(500, "Error")
        }
    }
}