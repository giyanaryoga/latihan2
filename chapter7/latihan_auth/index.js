const express = require('express');
const app = express.Router();
const session = require('express-session');
const flash = require('express-flash');
const { PORT = 8000 } = process.env;
const authRouter = require('./routers/')

app.set('view engine', 'ejs')

app.use(express.urlencoded({extended: false}))

app.use(authRouter)

app.listen(PORT, () => {
    console.log("Server started on port ", PORT)
})