'use strict';
const {
  Model
} = require('sequelize');

const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken');

module.exports = (sequelize, DataTypes) => {
  class user extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }

    

    static #encrypt = (password) => {
      return bcrypt.hashSync(password, 10)
    }

    static register = (username, password) => {
      const encryptedPassword = this.#encrypt(password)
      console.log(encryptedPassword)
      this.create({username, password : encryptedPassword})
    }

    static authentication = async (username, password) => {
      let user = await this.findOne({where : {username} })
      if (user == undefined) return false

      if (!bcrypt.compareSync(password, user.password)){
        return Promise.reject("Username / password is incorrect")
      }
      return Promise.resolve(user)
    }
  };
  user.init({
    username: DataTypes.STRING,
    password: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'user',
  });
  return user;
};