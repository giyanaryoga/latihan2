const express = require('express')
const app = express()
// const {Article} = require('./models')
const articleRouter = require('./routers/articleRouters')

// app.use(express.json())

app.use(
    express.urlencoded({
        extended: false
    })
)

app.set('view engine', 'ejs')

app.use(articleRouter)

// app.put('/api/article/:id', (req, res) => {
//     const {title, body, approved} = req.body

//     if (title === undefined || body === undefined || approved === undefined){
//         res.status(400).json("Beberapa field kosong")
//         return
//     }

//     const query = {
//         where: {id: req.params.id}
//     }
    
//     Article.update({
//         title: req.body.title,
//         body: req.body.body,
//         approved: req.body.approved
//     }, query).then(() => {
//         res.status(201).json("OK")
//     }).catch(err => {
//         res.status(422).json("Tidak dapat mengupdate data")
//     })

//     return
// })

// app.delete('/api/article/:id', (req, res) => {
//     Article.destroy({
//         where: {
//             id: req.params.id
//         }
//     }).then(() => res.status(200).json("OK"))
//     return
// })

app.listen(3000, () => {
    console.log("Server is running")
})