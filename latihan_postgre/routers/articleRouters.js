const express = require('express')
const articleRouter = express.Router()
const {Article} = require('../models')

articleRouter.get('/article/create', (req, res) => {
    res.render('articles/create')
    return
})

articleRouter.get('/article', (req, res) => {
    Article.findAll().then(articles => {
        res.render('articles/index', {
            articles
        })
    })
    return
})

articleRouter.get('/article/:id', (req, res) => {
    Article.findOne({
        where: {id : req.params.id}
    }).then(articles => {
        res.render('articles/show', {
            articles
        })
    })
    return
})

articleRouter.post('/article', (req, res) => {
    
    const {title, body} = req.body

    if (title === undefined || body === undefined){
        res.status(400).json("Beberapa field kosong")
        return
    }
    
    Article.create ({
        title: req.body.title,
        body: req.body.body,
        approved: false
    })
     .then(article => {
        res.status(200).json("Artikel berhasil dibuat!")
     }).catch(err => {
         console.log(err)
         res.status(500).json("Internal server error")
     })
    return
})

module.exports = articleRouter