const express = require('express')
const router = express.Router()

router.post('/customer', (req, res) => {
    res.send('Data customer posted')
})

router.get('/customer', (req, res) => {
    res.send('Semua data customer')
})

router.put('/customer', (req, res) => {
    res.send('Mengubah data customer')
})

router.delete('/customer', (req, res) => {
    res.send('Mendelete data customer')
})

module.exports = router