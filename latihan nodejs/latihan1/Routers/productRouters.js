const express = require('express')
const router = express.Router()

let dataProduct = [
    {
        namaProduct : "Kursi", 
        jumlahProduct : 12
    },
    {
        namaProduct : "Sofa",
        jumlahProduct : 15
    }
]

router.post('/product', (req, res) => {
    const namaProduct = req.body.namaProduct
    const jumlahProduct = req.body.jumlahProduct

    //Destructive Method
    // const {namaProduct, jumlahProduct} = req.body

    if (namaProduct === undefined || jumlahProduct === undefined){
        res.send("Bad Gateway")
        return
    }

    console.log(namaProduct);
    console.log(jumlahProduct)
    
    dataProduct.push({
        namaProduct : namaProduct,
        jumlahProduct : jumlahProduct
    })
    
    res.send('Inserted')
})

router.get('/product', (req, res) => {
    res.send('Semua data product')
})

router.put('/product', (req, res) => {
    res.send('Mengubah data produk')
})

router.delete('/product', (req, res) => {
    res.send('Mendelete data product')
})

module.exports = router