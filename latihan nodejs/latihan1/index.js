const express = require('express')
const app = express()
// const productRouters = require('./Routers/productRouters')
// const customerRouters = require('./Routers/customerRouters')


app.set('view engine', 'ejs')

app.get('/', (req, res) => {
    res.render('index')
})

app.get('/product', (req, res) => {
    const namaProduct = req.query.product
    const product = req.query.namaProduct

    res.render('product', {namaProduct,product})
})

// app.get('/product/:namaProduct', (req, res) => {
//     const parameter = req.params.namaProduct

//     if (parameter == 'laptop'){
//         const namaProduct = parameter
//         const product = 'Laptop ROG'

//         res.render('product', {namaProduct, product})
//     } else if (parameter == 'bangku'){
//         const namaProduct = parameter
//         const product = 'Bangku Ergonomis'

//         res.render('product', {namaProduct, product})
//     }
// })

app.listen(8000, console.log("Server is running!"))


/* app.use(function (req,res,next) {
    console.log('Request Method: ', req.method)
    next()
})

app.use(express.json())

app.get('/', (req,res) => {
    res.send("Hello World")
})

app.use(productRouters)
app.use(customerRouters) */

/* app.get('/user', (req,res) => {
    res.send("This is user endpoint")
})

app.get('/product', (req,res) => {
    res.send("This is product endpoint")
}) */



// const http = require ("http");
// //const fs = require ("fs");



// function onRequest(request, response) {
    
//     // response.writeHead(200, {"Content-Type" : "text/html"})
//     // fs.readFile("./index.html", null, (error, data) =>{
//     //     if (error){
//     //         response.writeHead(500, {"Content-Type" : "text/html"})
//     //         response.writeHead("Not Found")
//     //     } else {
//     //         response.write(data)
//     //     }
//     //     response.end()
//     // });

//     const data = {
//         nama : "Giyanaryoga Puguh",
//         prodi : "Sistem Informasi"
//     }

//         console.log(data.prodi)
//         console.log(data.nama)

//         response.write(JSON.stringify(data))

// }

// http.createServer(onRequest).listen(8085)